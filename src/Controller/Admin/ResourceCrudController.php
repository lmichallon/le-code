<?php

namespace App\Controller\Admin;

use App\Entity\Resource;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class ResourceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Resource::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Id')
                ->onlyOnIndex(),
            TextField::new('title', 'Titre')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Titre de la ressource',
                    'maxLength' => '50']),
            TextField::new('description', 'Description')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Description de la ressource',
                    'maxLength' => '255']),
            TextField::new('source', 'Source')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Lien de la ressource',
                    'maxLength' => '255']),
            AssociationField::new('categories', 'Catégories'),
            AssociationField::new('tags', 'Tags'),
            AssociationField::new('format', 'Format'),
            AssociationField::new('comments', 'Commentaires')
                ->onlyOnIndex(),
        ];
    }

    public function configureActions (Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setLabel('Ajouter une ressource');
            });
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Ressource')
            ->setEntityLabelInPlural('Ressources')
            ->setPageTitle('new', 'Ajouter une ressource')
            ->setPageTitle('edit', 'Éditer une ressource')
            ;
    }
}
