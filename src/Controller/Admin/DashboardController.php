<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\Resource;
use App\Entity\Format;
use App\Entity\Category;
use App\Entity\Tag;
use App\Entity\User;
use App\Repository\CommentRepository;
use App\Repository\ResourceRepository;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;

class DashboardController extends AbstractDashboardController
{
    protected $userRepository;
    protected $commentRepository;
    protected $resourceRepository;

    public function __construct(
        UserRepository $userRepository,
        CommentRepository $commentRepository,
        ResourceRepository $resourceRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->commentRepository = $commentRepository;
        $this->resourceRepository = $resourceRepository;
    }

    /**
     * @Route("/735vxj_admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('bundles/EasyAdminBundle/welcome.html.twig', [
            'countUsers' => $this->userRepository->countUsers(),
            'countResources' => $this->resourceRepository->countResources(),
            'countComments' => $this->commentRepository->countComments()
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Le code')
            ->renderSidebarMinimized()
            ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Ressources', 'fa fa-newspaper', Resource::class);
        yield MenuItem::linkToCrud('Formats', 'fa fa-shapes', Format::class);
        yield MenuItem::linkToCrud('Catégories', 'fa fa-tag', Category::class);
        yield MenuItem::linkToCrud('Tags', 'fa fa-tags', Tag::class);
        yield MenuItem::linkToCrud('Commentaires', 'fa fa-comments', Comment::class);
        yield MenuItem::linkToCrud('Utilisateurs', 'fa fa-users', User::class);
    }
}
