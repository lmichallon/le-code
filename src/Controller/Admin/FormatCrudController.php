<?php

namespace App\Controller\Admin;

use App\Entity\Format;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class FormatCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Format::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Id')
                ->onlyOnIndex(),
            TextField::new('name', 'Nom')
                ->setFormTypeOption('attr',
                    ['placeholder' => 'Nom du format',
                    'maxLength' => '30']),
        ];
    }

    public function configureActions (Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                return $action
                    ->setLabel('Ajouter un format');
            });
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Format')
            ->setEntityLabelInPlural('Formats')
            ->setPageTitle('new', 'Ajouter un format')
            ->setPageTitle('edit', 'Éditer un format')
            ;
    }
}
