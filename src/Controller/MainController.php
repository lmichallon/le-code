<?php

namespace App\Controller;

use App\Entity\Resource;
use App\Entity\User;
use App\Repository\ResourceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('pages/index.html.twig');
    }

    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @return Response
     */
    public function search(Request $request): Response
    {
        return $this->render('pages/search.html.twig');
    }

    /**
     * @Route("/ressource", name="resource")
     * @param Request $request
     * @return Response
     */
    public function resource(Request $request): Response
    {
        return $this->render('pages/resource.html.twig');
    }

    /**
     * @Route("/users/{username}", name="user_profile")
     * @param UserRepository $userRepository
     * @param string $username
     * @return Response
     */
    public function userProfile(UserRepository $userRepository, string $username): Response
    {
        if ($username === $this->getUser()->getUsername()) {
            $user = $userRepository->findOneBy(['username' => $username]);

            return $this->render('pages/profile.html.twig', [
                'user' => $user
            ]);
        } else {
            // REDIRECTION VERS LA PAGE DE PROFIL PERSO
            return $this->redirectToRoute('user_profile', [
                'username' => $this->getUser()->getUsername()
            ]);
        }
    }

    /**
     * @Route("/terms", name="terms")
     * @return Response
     */
    public function legalNotices(): Response
    {
        return $this->render('pages/terms.html.twig');

    }
}
