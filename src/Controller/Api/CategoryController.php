<?php

namespace App\Controller\Api;

use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api", name="api_")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/categories", name="categories")
     * @param CategoryRepository $categoryRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getAllCategories(CategoryRepository $categoryRepository, SerializerInterface $serializer): Response
    {
        return $this->json($categoryRepository->findAll(), 200, [], ['groups' => 'categories:read']);
    }
}
