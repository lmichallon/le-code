<?php

namespace App\Controller\Api;

use App\Repository\ResourceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api", name="api_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/users/{id}/bookmarks", name="user", methods={"GET"})
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param UserRepository $userRepository
     * @param SerializerInterface $serializer
     * @param int $id
     * @return Response
     */
    public function getUserBookmarks(Request $request,
                                     PaginatorInterface $paginator,
                                     UserRepository $userRepository,
                                     SerializerInterface $serializer,
                                     int $id): Response
    {
        // PAGINATION
        $resourcesPerPage = 6;
        $pagination = $paginator->paginate(
            $userRepository->find($id)->getBookmarks(),
            $request->query->getInt('page', 1), // récupère la page dans l'URL, si aucune : page 1 par défaut
            $resourcesPerPage
        );
        $totalPages = ceil($pagination->getTotalItemCount()/$resourcesPerPage);

        // RESSOURCES
        $resources = $serializer->serialize($pagination, 'json', ['groups' => 'resources:read']);

        // RÉPONSE
        return new JsonResponse([
            'resources' => json_decode($resources, true), // json_decode() car sinon retourne une string
            'totalPages' => $totalPages
        ], 200, [], false);
    }

    /**
     * @Route("/users/resources/{id}", name="add_bookmark", methods={"POST"})
     * @param UserRepository $userRepository
     * @param ResourceRepository $resourceRepository
     * @param EntityManagerInterface $entityManager
     * @param int $id
     * @return Response
     */
    public function createUserBookmark(UserRepository $userRepository,
                                       ResourceRepository $resourceRepository,
                                       EntityManagerInterface $entityManager,
                                       int $id): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $resource = $resourceRepository->find($id);

        $user->addBookmark($resource);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json(null, 201);
    }

    /**
     * @Route("/users/resources/{id}", name="remove_bookmark", methods={"DELETE"})
     * @param UserRepository $userRepository
     * @param ResourceRepository $resourceRepository
     * @param EntityManagerInterface $entityManager
     * @param int $id
     * @return Response
     */
    public function deleteUserBookmark(UserRepository $userRepository,
                                       ResourceRepository $resourceRepository,
                                       EntityManagerInterface $entityManager,
                                       int $id): Response
    {
        $user = $userRepository->find($this->getUser()->getId());
        $resource = $resourceRepository->find($id);

        $user->removeBookmark($resource);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json(null, 201);
    }

    /**
     * @Route("/users", name="delete_account", methods={"DELETE"})
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function deleteAccount(UserRepository $userRepository,
                                  EntityManagerInterface $entityManager): Response
    {
        $user = $userRepository->find($this->getUser()->getId());

        if ($user) {
            $entityManager->remove($user);
            $entityManager->flush();
        }

        $session = new Session();
        $session->invalidate();

        return new Response(null, 204);
    }
}
