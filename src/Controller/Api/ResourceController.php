<?php

namespace App\Controller\Api;

use App\Repository\ResourceRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api", name="api_")
 */
class ResourceController extends AbstractController
{
    /**
     * @Route("/ressources", name="resources", methods={"GET"})
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param ResourceRepository $resourceRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getAllResources(Request $request,
                                    PaginatorInterface $paginator,
                                    ResourceRepository $resourceRepository,
                                    SerializerInterface $serializer): Response
    {
        // PAGINATION
        $resourcesPerPage = 6;
        $pagination = $paginator->paginate(
            array_reverse($resourceRepository->findAll()),
            $request->query->getInt('page', 1), // récupère la page dans l'URL, si aucune : page 1 par défaut
            $resourcesPerPage
        );
        $totalPages = ceil($pagination->getTotalItemCount()/$resourcesPerPage);

        // RESSOURCES
        $resources = $serializer->serialize($pagination, 'json', ['groups' => 'resources:read']);

        // RÉPONSE
         return new JsonResponse([
             'resources' => json_decode($resources), // json_decode() car sinon retourne une string
             'totalPages' => $totalPages
         ], 200, [], false);
    }

    /**
     * @Route("/ressources/categories/{name}", name="resources_by_category", methods={"GET"})
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param ResourceRepository $resourceRepository
     * @param SerializerInterface $serializer
     * @param string $name
     * @return Response
     */
    public function getResourcesByCategory(Request $request,
                                           PaginatorInterface $paginator,
                                           ResourceRepository $resourceRepository,
                                           SerializerInterface $serializer,
                                           string $name): Response
    {
        // PAGINATION
        $resourcesPerPage = 6;
        $pagination = $paginator->paginate(
            array_reverse($resourceRepository->findByCategory($name)),
            $request->query->getInt('page', 1), // récupère la page dans l'URL, si aucune : page 1 par défaut
            $resourcesPerPage
        );
        $totalPages = ceil($pagination->getTotalItemCount()/$resourcesPerPage);

        // RESSOURCES
        $resources = $serializer->serialize($pagination, 'json', ['groups' => 'resources:read']);

        // RÉPONSE
        return new JsonResponse([
            'resources' => json_decode($resources), // json_decode() car sinon retourne une string
            'totalPages' => $totalPages
        ], 200, [], false);
    }

    /**
     * @Route("/ressources/tags/{name}", name="resources_by_tag", methods={"GET"})
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param ResourceRepository $resourceRepository
     * @param SerializerInterface $serializer
     * @param string $name
     * @return Response
     */
    public function getResourcesByTag(Request $request,
                                      PaginatorInterface $paginator,
                                      ResourceRepository $resourceRepository,
                                      SerializerInterface $serializer,
                                      string $name): Response
    {

        // PAGINATION
        $resourcesPerPage = 6;
        $pagination = $paginator->paginate(
            array_reverse($resourceRepository->findByTag($name)),
            $request->query->getInt('page', 1), // récupère la page dans l'URL, si aucune : page 1 par défaut
            $resourcesPerPage
        );
        $totalPages = ceil($pagination->getTotalItemCount()/$resourcesPerPage);

        // RESSOURCES
        $resources = $serializer->serialize($pagination, 'json', ['groups' => 'resources:read']);

        // RÉPONSE
        return new JsonResponse([
            'resources' => json_decode($resources), // json_decode() car sinon retourne une string
            'totalPages' => $totalPages
        ], 200, [], false);
    }

    /**
     * @Route("/ressources/{id}", name="resource_by_id", methods={"GET"})
     * @param ResourceRepository $resourceRepository
     * @param int $id
     * @return Response
     */
    public function getOneResource(ResourceRepository $resourceRepository, int $id): Response
    {
        return $this->json($resourceRepository->find($id), 200, [], ['groups' => 'resources:read']);
    }
}
