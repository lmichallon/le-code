<?php

namespace App\Controller\Api;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ResourceRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/comments", name="new_comment", methods={"POST"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @param ResourceRepository $resourceRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function createComment(Request $request,
                                  UserRepository $userRepository,
                                  ResourceRepository $resourceRepository,
                                  EntityManagerInterface $entityManager): Response
    {
        $requestContent = json_decode($request->getContent(), true);
        $author = $userRepository->find($this->getUser()->getId());
        $resource = $resourceRepository->find($requestContent['resource_id']);

        $comment = new Comment();
        $comment->setAuthor($author);
        $comment->setResource($resource);
        $comment->setContent($requestContent['content']);
        $comment->setCreatedAt(new \DateTime());

        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->json(null, 201);
    }

    /**
     * @Route("/comments/{id}", name="delete_comment", methods={"DELETE"})
     * @param EntityManagerInterface $entityManager
     * @param CommentRepository $commentRepository
     * @param int $id
     * @return Response
     */
    public function deleteComment(CommentRepository $commentRepository,
                                  EntityManagerInterface $entityManager,
                                  int $id): Response
    {
        $comment = $commentRepository->find($id);

        $author = $comment->getAuthor()->getId();
        $user = $this->getUser()->getId();

        if (($comment) && ($author === $user)) {
            $entityManager->remove($comment);
            $entityManager->flush();
            return new Response(null, 204);
        }
        else if ($comment) { return new Response(null, 403); }
        else { return new Response(null, 404); }
    }
}
