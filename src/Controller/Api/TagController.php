<?php

namespace App\Controller\Api;

use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api", name="api_")
 */
class TagController extends AbstractController
{
    /**
     * @Route("/tags", name="tags", methods={"GET"})
     * @param TagRepository $tagRepository
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function getAllTags(TagRepository $tagRepository, SerializerInterface $serializer): Response
    {
        return $this->json($tagRepository->findAll(), 200, [], ['groups' => 'tags:read']);
    }
}
