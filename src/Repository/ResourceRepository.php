<?php

namespace App\Repository;

use App\Entity\Resource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Resource|null find($id, $lockMode = null, $lockVersion = null)
 * @method Resource|null findOneBy(array $criteria, array $orderBy = null)
 * @method Resource[]    findAll()
 * @method Resource[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResourceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Resource::class);
    }

    public function countResources()
    {
        $queryBuilder = $this->createQueryBuilder('r');
        $queryBuilder->select('COUNT(r.id) as value');

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $category
     * @return Resource[] Returns an array of Resource objects
     */
    public function findByCategory($category): array
    {
        $qb = $this->createQueryBuilder('r')
            ->join('r.categories', 'c')
            ->setParameter('category', $category)
            ->andWhere('c.name = :category');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $tag
     * @return Resource[] Returns an array of Resource objects
     */
    public function findByTag($tag): array
    {
        $qb = $this->createQueryBuilder('r')
            ->join('r.tags', 't')
            ->setParameter('tag', $tag)
            ->andWhere('t.name = :tag');

        return $qb->getQuery()->getResult();
    }
}
