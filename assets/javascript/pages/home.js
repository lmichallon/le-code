import getResources from "../functions/getResources";

// RÉCUPÉRATION DE LA PAGE DANS LES PARAMÈTRES DE L'URL
const URLparameters = new URLSearchParams(window.location.search);
let page = URLparameters.get('page');

if ( page === null) { page = 1 } // SI PAS DE PAGE ON ATTRIBUE LA VALEUR 1

// RÉCUPÉRATION DES RESSOURCES
getResources(`/api/ressources?page=${page}`);
