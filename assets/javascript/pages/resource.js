import fetcher from '../functions/fetcher';
import displayResource from '../functions/displayResource';
import { displayComment, addComment } from '../functions/manageComments';
import displaySignUpModal from '../functions/displaySignUpModal';

// RÉCUPÉRATION DES PARAMÈTRES DE L'URL POUR DÉTERMINER CELLES DES REQUÊTES API
const URLparameters = new URLSearchParams(window.location.search);
const resourceID = URLparameters.get('id');

// AFFICHAGE DE LA RESSOURCE ET DE SES COMMENTAIRES
const getOneResource = async () => {
    let data = await fetcher(`/api/ressources/${resourceID}`);

    if (data === undefined) {
        // GESTION DE L'ERREUR
    } else {
        const resourceContainer = document.querySelector('.resource-container');
        resourceContainer.style.columnCount = 1; // GESTION DU RESPONSIVE DESIGN

        // AFFICHAGE DE LA RESSOURCE
        displayResource(data, resourceContainer);

        // AFFICHAGE DE SES COMMENTAIRES
        for (const comment of data.comments) {
            displayComment(comment, document.querySelector('#commentsContainer'));
        }
    }
}

// CRÉATION D'UN COMMENTAIRE
document.querySelector('#newCommentForm').addEventListener('submit', (event) => {
    event.preventDefault();

    // SI UTILISATEUR CONNECTÉ : CRÉATION DU COMMENTAIRE
    if (document.querySelector('nav').getAttribute('data-user') !== 'anonymous') {
        addComment(resourceID, document.querySelector('#commentContent').value);
        window.location.reload();
    }
    // SI UTILISATEUR NON-CONNECTÉ : MODAL QUI INVITE À CRÉER UN COMPTE
    else { displaySignUpModal(); }
});

getOneResource();
