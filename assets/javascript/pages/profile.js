import getResources from "../functions/getResources";
import axios from "axios";

// RÉCUPÉRATION DE L'ID DE L'UTILISATEUR CONNECTÉ
const userID = document.querySelector('nav').getAttribute('data-user');

// RÉCUPÉRATION DE LA PAGE DANS LES PARAMÈTRES DE L'URL
const URLparameters = new URLSearchParams(window.location.search);
let page = URLparameters.get('page');

if ( page === null) { page = 1 } // SI PAS DE PAGE ON ATTRIBUE LA VALEUR 1

// RÉCUPÉRATION DES RESSOURCES
getResources(`/api/users/${userID}/bookmarks?page=${page}`);

// BOUTON DE SUPPRESSION DU COMPTE
const deleteAccountButton = document.querySelector('#deleteAccount');

deleteAccountButton.addEventListener('click', (event) => {
    axios.delete(`/api/users`);
    window.location.href = '/'; // REDIRECTION VERS LA PAGE D'ACCUEIL
});