import getResources from '../functions/getResources';

// RÉCUPÉRATION DES PARAMÈTRES DE L'URL
const URLparameters = new URLSearchParams(window.location.search);
const category = URLparameters.get('category');
const tag = URLparameters.get('tag');
let page = URLparameters.get('page');

// DÉTERMINATION ET AFFICHAGE DU TITRE DE LA RECHERCHE
const displaySearchTitle = () => {
    if (URLparameters.has('category')) {
        document.querySelector('#searchTitle').innerHTML = `Résultats de recherche pour "${category}" :`
    } else if (URLparameters.has('tag')){
        document.querySelector('#searchTitle').innerHTML = `Résultats de recherche pour "${tag}" :`
    }
}

// DÉTERMINATION DE L'URL DU FETCH
const getFetchURL = () => {
    if (page === null) { page = 1 }
    if (URLparameters.has('category')) { return `/api/ressources/categories/${category}?page=${page}`; }
    else if (URLparameters.has('tag')) { return `/api/ressources/tags/${tag}?page=${page}`; }
}

// AFFICHAGE DES RÉSULTATS SELON L'URL RÉCUPÉRÉE
const displaySearchResults = async () => {
    let fetchURL = await getFetchURL();
    getResources(fetchURL);
}

displaySearchTitle();
displaySearchResults();