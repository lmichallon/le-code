import '../scss/app.scss';

// GESTION DE LA BARRE DE NAVIGATION MOBILE
const toggle = document.querySelector('#nav-toggle');
const links = document.querySelector('.nav-links');

// AU CLIC SUR LE BURGER MENU
toggle.addEventListener('click', (e) => {
    if (toggle.className === 'fa fa-bars') {
        links.style.display = 'block';
        toggle.className = 'fa fa-times';
    } else {
        links.style.display = 'none';
        toggle.className = 'fa fa-bars';
    }
});

// GESTION DE L'AFFICHAGE DES LIENS SELON LA LARGEUR DE LA FENÊTRE
window.addEventListener('resize', () => {
    if (window.innerWidth >= 767) { links.style.display = 'flex'; }
    else { links.style.display = 'none'; }
});

// GESTION DE LA BARRE RECHERCHE
const searchBar = document.querySelector('#searchBar');
const searchIcon = document.querySelector('#searchIcon');

// AU CLIC SUR L'ICÔNE DE RECHERCHE
searchIcon.addEventListener('click', (event) => {
    window.location.href = `/search?tag=${searchBar.value}&page=1`; // REDIRECTION
})

// À LA PRESSION SUR LA TOUCHE ENTRÉE
searchBar.addEventListener('keyup', (event) => {
    if (event.keyCode === 13) { window.location.href = `/search?tag=${searchBar.value}&page=1`; } // REDIRECTION
})