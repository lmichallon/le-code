import displayBookmarkIcon from './manageBookmarks';

// INJECTION D'UNE RESSOURCE DANS LE DOM
const displayResource = (data, DOMTarget) => {

    // RÉCUPÉRATION DES DONNÉES DE L'UTILISATEUR
    const user = document.querySelector('nav').getAttribute('data-user');

    // CONTENEUR
    const resourceContainer = document.createElement('article');
    resourceContainer.setAttribute('id', `${data.id}`);
    resourceContainer.className = 'resource';
    DOMTarget.appendChild(resourceContainer);

    // BOOKMARK
    displayBookmarkIcon(data.bookmarks, user, resourceContainer);

    // TITRE
    const resourceLink = document.createElement('a');
    resourceLink.setAttribute('href', `${data.source}`);
    resourceLink.setAttribute('target', '_blank');
    resourceLink.setAttribute('rel', 'noopener noreferrer');
    resourceContainer.appendChild(resourceLink);

    const resourceTitle = document.createElement('h2');
    resourceTitle.innerText = `${data.title}`;
    resourceLink.appendChild(resourceTitle);

    // DESCRIPTION
    const resourceDescription = document.createElement('p');
    resourceDescription.innerText = `${data.description}`;
    resourceContainer.appendChild(resourceDescription);

    // CATÉGORIES
    for (const category of data.categories) {

        const resourceCategory = document.createElement('span');
        resourceCategory.className = 'label label--category';
        resourceCategory.innerText = `${category.name}`;
        resourceContainer.appendChild(resourceCategory);

        // REDIRECTION AU CLIC SUR UNE CATÉGORIE
        resourceCategory.addEventListener('click', (e) => {
            window.location.href = `/search?category=${category.name}&page=1`
        });
    }

    // TAGS
    for (const tag of data.tags) {

        const resourceTag = document.createElement('span');
        resourceTag.className = 'label label--tag';
        resourceTag.innerText = `${tag.name}`;
        resourceContainer.appendChild(resourceTag);

        // REDIRECTION AU CLIC SUR UN TAG
        resourceTag.addEventListener('click', (e) => {
            window.location.href = `/search?tag=${tag.name}&page=1`;
        });
    }

    // COMMENTAIRES
    const resourceComments = document.createElement('a');
    resourceComments.setAttribute('href', `/ressource?id=${data.id}`)
    resourceComments.innerText = `${data.comments.length} commentaires`;
    resourceContainer.appendChild(resourceComments);
}

export default displayResource;