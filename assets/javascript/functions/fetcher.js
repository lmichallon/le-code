import axios from 'axios';

// RÉCUPÉRATION DE DONNÉES DEPUIS L'API
const fetcher = async (URL) => {
    try {
        const response = await axios.get(`${URL}`);
        return response.data;
    }
    catch(error) {
        return undefined;
    }
}

export default fetcher;