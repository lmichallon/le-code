import axios from "axios";

// INJECTION DES COMMENTAIRES D'UNE RESSOURCE DANS LE DOM
const displayComment = (data, DOMTarget) => {

    // RÉCUPÉRATION DES DONNÉES DE L'UTILISATEUR
    const user = document.querySelector('nav').getAttribute('data-user');

    // CONTENEUR
    const commentContainer = document.createElement('article');
    commentContainer.setAttribute('id', `${data.id}`);
    commentContainer.className = 'comment';
    DOMTarget.appendChild(commentContainer);

    // SI L'UTILISATEUR CONNECTÉ EST L'AUTEUR DU COMMENTAIRE : AFFICHAGE DU BOUTON DE SUPPRESSION
    if (parseFloat(user) === data.author.id) {
        const deleteIcon = document.createElement('i');
        deleteIcon.className = 'fa fa-trash';
        commentContainer.appendChild(deleteIcon);

        // GESTION DU CLIC SUR LE BOUTON DE SUPPRESSION
        deleteIcon.addEventListener('click', (event) => {
            deleteComment(data.id);
            window.location.reload();
        });
    }

    // AUTEUR
    const commentAuthor = document.createElement('p');
    commentContainer.setAttribute('id', `${data.id}`);
    commentAuthor.innerText = `${data.author.username}`;
    commentContainer.appendChild(commentAuthor);

    // CONTENU
    const commentContent = document.createElement('p');
    commentContent.innerText = `${data.content}`;
    commentContainer.appendChild(commentContent);

    // DATE
    const commentDate = document.createElement('p');
    commentDate.innerText = `${getFormattedDate(data.created_at)}`;
    commentContainer.appendChild(commentDate);
}

// CRÉATION D'UN COMMENTAIRE SOUS UNE RESSOURCE
const addComment = (resourceID, commentContent) => {
    axios.post('/api/comments', {
        resource_id: resourceID,
        content: commentContent,
    });
}

// SUPPRESSION D'UN COMMENTAIRE
const deleteComment = (id) => {
    axios.delete(`/api/comments/${id}`);
}

// FORMATAGE DE LA DATE
const getFormattedDate = (dateTime) => {

    // DÉCLARATION D'UNE NOUVELLE DATE À PARTIR DU DATETIME RÉCUPÉRÉ
    const date = new Date(dateTime);

    // DÉCOUPAGE DE LA DATE RÉCUPÉRÉE
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let hours = date.getHours();
    let minutes = date.getMinutes();

    // SI RÉSULTAT À UN CHIFFRE : UN ZÉRO LE PRÉCÈDERA
    if (day < 10) { day = `0${day}`}
    if (month < 10) { month = `0${month}`}
    if (hours < 10) { hours = `0${hours}`}
    if (minutes < 10) { minutes = `0${minutes}`}

    // RETOURNE
    return `Le ${day}/${month}/${year} à ${hours}:${minutes}`;
}

export { displayComment, addComment, deleteComment };