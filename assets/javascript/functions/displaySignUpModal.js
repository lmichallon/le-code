
const displaySignUpModal = () => {

    const body = document.querySelector('body');

    const modalBackdrop = document.createElement('div');
    modalBackdrop.className = 'modal-backdrop';
    body.prepend(modalBackdrop);

    const modal = document.createElement('div');
    modal.className = 'modal';
    modalBackdrop.appendChild(modal);

    const closeIcon = document.createElement('span');
    closeIcon.className = 'fa fa-times';
    modal.appendChild(closeIcon);

    const modalContent = document.createElement('p');
    modalContent.innerText = 'Tu dois posséder un compte pour utiliser cette fonctionnalité !'
    modal.appendChild(modalContent);

    const signUpButton = document.createElement('a');
    signUpButton.className = 'btn';
    signUpButton.innerText = 'S\'inscrire';
    signUpButton.setAttribute('href', '/inscription');
    modal.appendChild(signUpButton);

    const signInLink = document.createElement('a');
    signInLink.innerText = 'Se connecter';
    signInLink.setAttribute('href', '/connexion');
    modal.appendChild(signInLink);

    closeIcon.addEventListener('click', (event) => {
        modalBackdrop.style.display = 'none';
        modalBackdrop.parentNode.removeChild(modalBackdrop);
    });
}

export default displaySignUpModal;