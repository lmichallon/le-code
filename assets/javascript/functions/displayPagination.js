// INJECTION DE LA PAGINATION DANS LE DOM
const displayPagination = (totalPages, DOMTarget) => {

    for (let i = 1; i <= totalPages; i += 1) {
        // CRÉATION DES BOUTONS
        const button = document.createElement('button');
        button.innerText = `${i}`;
        DOMTarget.append(button);

        // RÉCUPÉRATION DE LA PAGE COURANTE DANS L'URL
        const URLparameters = new URLSearchParams(window.location.search);
        let page = parseFloat(URLparameters.get('page'));
        if ( isNaN(page) === true ) { page = 1 } // SI PAS DE PAGE DANS L'URL ON LUI DONNE LA VALEUR 1

        // DISTINCTION DE LA PAGE ACTIVE
        if (page === i) { button.classList.add('active'); }

        // GESTION DU CLIC SUR UNE PAGE
        button.addEventListener('click', (event) => {
            // DÉDUCTION DE L'URL DE REDIRECTION
            URLparameters.set('page', `${event.target.innerHTML}`);
            // REDIRECTION
            window.location.href = `${window.location.pathname}?${URLparameters.toString()}`;
        });
    }
}

export default displayPagination;