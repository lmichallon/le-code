import fetcher from './fetcher';
import displayResource from './displayResource';
import displayPagination from './displayPagination';

const getResources = async (fetchURL) => {
    let data = await fetcher(`${fetchURL}`);

    if (data === undefined) {
        // GESTION DE L'ERREUR
    } else {
        const resourceContainer = document.querySelector('.resource-container');

        // SI AUCUNE RESSOURCE TROUVÉE
        if (data.resources.length === 0) {
            const noResourceFound = document.createElement('p');
            noResourceFound.innerText = 'Aucune ressource trouvée.';
            resourceContainer.appendChild(noResourceFound);
            resourceContainer.style.columnCount = '1'; // AFFICHAGE SUR UNE SEULE COLONNE
        } else {
            // AFFICHAGE DES RESSOURCES
            for (const resource of Object.values(data.resources)) {
                displayResource(resource, resourceContainer);
            }

            // GESTION DU RESPONSIVE DESIGN
            if (Object.values(data.resources).length <= 5) {
                resourceContainer.style.display = 'flex';
                resourceContainer.style.justifyContent = 'center';
                resourceContainer.style.alignItems = 'start';
            }

            // AFFICHAGE DE LA PAGINATION
            if (data.totalPages > 1) {
                displayPagination(data.totalPages, document.querySelector('#paginationContainer'));
            }
        }
    }
}

export default getResources;