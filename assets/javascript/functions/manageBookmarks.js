import axios from "axios";
import displaySignUpModal from "./displaySignUpModal";

// DÉTERMINE SI LA RESSOURCE EST LIKÉE PAR L'UTILISATEUR
const isBookmarked = (bookmarks, user) => {
    if (bookmarks === undefined) {
        return true;
    } else {
        for (const bookmark of bookmarks) {
            if (bookmark.id === parseInt(user)) { return true; }
        }
        return false;
    }
}

// DÉTERMINE L'ICÔNE À AFFICHER
const setIcon = (bookmarked, DOMTarget) => {
    if (bookmarked === true) { DOMTarget.className = 'fa fa-bookmark'; }
    else { DOMTarget.className = 'fa fa-bookmark-o'; }
}

// AJOUT DU BOOKMARK EN BASE DE DONNÉES
const addToBookmarks = (resourceID) => {
    axios.post(`/api/users/resources/${resourceID}`, {
        resource_id: resourceID,
    })
}

// RETRAIT DU BOOKMARK DE LA BASE DE DONNÉES
const removeFromBookmarks = (resourceID) => {
    axios.delete(`/api/users/resources/${resourceID}`, {
        resource_id: resourceID,
    })
}

const displayBookmarkIcon = (bookmarks, user, DOMTarget) => {
    // INITIALISATION DE L'ICÔNE
    const resourceBookmarkIcon = document.createElement('i');
    DOMTarget.append(resourceBookmarkIcon);
    let bookmarked = isBookmarked(bookmarks, user);
    setIcon(bookmarked, resourceBookmarkIcon);

    // GESTION DU CLIC SUR L'ICÔNE
    resourceBookmarkIcon.addEventListener('click', (event) => {

        // SI UTILISATEUR CONNECTÉ : CRÉATION DU COMMENTAIRE
        if (document.querySelector('nav').getAttribute('data-user') !== 'anonymous') {
            // AJOUT OU RETRAIT DU BOOKMARK EN BASE DE DONNÉES
            if (bookmarked === true) { removeFromBookmarks(DOMTarget.getAttribute('id')); }
            else { addToBookmarks(DOMTarget.getAttribute('id')); }

            // MISE À JOUR DE L'ICÔNE
            bookmarked = !bookmarked;
            setIcon(bookmarked, resourceBookmarkIcon);
        }
        // SI UTILISATEUR NON-CONNECTÉ : MODAL QUI INVITE À CRÉER UN COMPTE
        else { displaySignUpModal(); }
    });
}

export default displayBookmarkIcon;